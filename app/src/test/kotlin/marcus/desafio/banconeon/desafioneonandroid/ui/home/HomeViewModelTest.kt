package marcus.desafio.banconeon.desafioneonandroid.ui.home

import com.orhanobut.hawk.Hawk
import marcus.desafio.banconeon.desafioneonandroid.data.api.Api
import marcus.desafio.banconeon.desafioneonandroid.data.api.Local
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment

@RunWith(RobolectricTestRunner::class)
class HomeViewModelTest {

    private val api = mock(Api::class.java)
    private val local = mock(Local::class.java)
    private lateinit var viewModel: HomeViewModel

    @Before
    fun setup() {
        Hawk.init(RuntimeEnvironment.application).build()
        viewModel = HomeViewModel(api, local)
    }

    @Test
    fun testIfTokenIsSaved() {
        val token = "1234567890"

        viewModel.saveToken(token)
        assertEquals(token, viewModel.getToken())
    }
}