package marcus.desafio.banconeon.desafioneonandroid.ui.send

import com.orhanobut.hawk.Hawk
import com.squareup.moshi.Moshi
import marcus.desafio.banconeon.desafioneonandroid.data.api.Api
import marcus.desafio.banconeon.desafioneonandroid.data.api.Local
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment

@RunWith(RobolectricTestRunner::class)
class SendViewModelTest {

    private val api = mock(Api::class.java)
    private val local = mock(Local::class.java)
    private lateinit var viewModel: SendMoneyViewModel

    @Before
    fun setup() {
        Hawk.init(RuntimeEnvironment.application).build()
        viewModel = SendMoneyViewModel(api, local, Moshi.Builder().build())
    }

    @Test
    fun testIfFormatMoneyIsCorrect() {
        val value = "R$159,00"
        val formattedValue = viewModel.formatMoney(value)

        assertEquals(formattedValue, "159.00")
    }
}