package marcus.desafio.banconeon.desafioneonandroid.ui.send

import android.arch.lifecycle.MutableLiveData
import com.squareup.moshi.Moshi
import concrete.marcussouza.desafioandroid.ui.BaseViewModel
import concrete.marcussouza.desafioandroid.util.ResourceLiveData
import concrete.marcussouza.desafioandroid.util.toErrorHandler
import marcus.desafio.banconeon.desafioneonandroid.data.api.Api
import marcus.desafio.banconeon.desafioneonandroid.data.api.Local
import marcus.desafio.banconeon.desafioneonandroid.data.model.Friend
import marcus.desafio.banconeon.desafioneonandroid.data.model.FriendList
import marcus.desafio.banconeon.desafioneonandroid.data.model.SendMoney
import marcus.desafio.banconeon.desafioneonandroid.util.Util
import java.io.InputStream
import javax.inject.Inject


class SendMoneyViewModel @Inject constructor(api: Api, local: Local, private val moshi: Moshi) : BaseViewModel(api, local) {

    val friendListResult = MutableLiveData<List<Friend>>()
    val sendMoneyResult = ResourceLiveData<Boolean>()
    val validValue = MutableLiveData<Boolean>()

    fun getFriendList(openRawResource: InputStream) {
        loading.postValue(true)
        val friendString = Util.inputStreamToString(openRawResource)

        val jsonAdapter = moshi.adapter(FriendList::class.java)
        val friendList = jsonAdapter.fromJson(friendString)

        friendListResult.postValue(friendList.friendList)
        loading.postValue(false)
    }

    fun sendMoneyToFriend(clientId: Int, money: String) {
        val sendMoney = SendMoney(clientId, getToken(), formatMoney(money).toDouble())

        if (sendMoney.value == 0.00) {
            validValue.postValue(false)
            return
        }

        api.sendMoney(sendMoney)
                .doOnSubscribe { loading.postValue(true) }
                .doFinally { loading.postValue(false) }
                .toErrorHandler()
                .subscribeLiveData(this, sendMoneyResult)
    }

    fun getToken(): String = local.getToken()

    fun formatMoney(money: String): String {
        var cleanMoney = money.replace("[R$.]".toRegex(), "")
        cleanMoney = cleanMoney.replace(",", ".")
        return cleanMoney
    }
}