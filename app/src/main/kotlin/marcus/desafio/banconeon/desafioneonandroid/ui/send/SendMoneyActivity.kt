package marcus.desafio.banconeon.desafioneonandroid.ui.send

import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.*
import android.view.View
import android.widget.FrameLayout
import concrete.marcussouza.desafioandroid.ui.BaseActivity
import concrete.marcussouza.desafioandroid.ui.BaseRecyclerAdapter
import concrete.marcussouza.desafioandroid.util.bindView
import marcus.desafio.banconeon.desafioneonandroid.R
import marcus.desafio.banconeon.desafioneonandroid.data.model.Friend
import marcus.desafio.banconeon.desafioneonandroid.util.MonetaryMask
import javax.inject.Inject


class SendMoneyActivity : BaseActivity(), BaseRecyclerAdapter.OnItemClickListener {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)
                .get(SendMoneyViewModel::class.java)
    }

    private val sendMoneyRecyclerAdapter by lazy { SendMoneyRecyclerAdapter() }

    private val toolbar: Toolbar by bindView(R.id.activity_send_money_toolbar)
    private val recyclerView: RecyclerView by bindView(R.id.activity_send_money_recycler)
    private val loadingContainer: FrameLayout by bindView(R.id.activity_send_loading_container)
    private val container: ConstraintLayout by bindView(R.id.activity_send_money_container)
    private lateinit var dialog: Dialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_send_money)
        setupToolbar()
        initObservers()
        initRecycler()
        initDialog()

        viewModel.getFriendList(applicationContext.resources.openRawResource(R.raw.friends_list))
    }

    private fun setupToolbar() {
        toolbar.background.alpha = 0
        setSupportActionBar(toolbar)
        supportActionBar?.title = resources.getString(R.string.send_money)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initObservers() {
        viewModel.loading.observe(this, Observer {
            changeLoadingVisibilty(it!!)
        })

        viewModel.friendListResult.observe(this, Observer {
            fillRecyclerView(it)
        })

        viewModel.sendMoneyResult.observe(this, Observer {
            if (!it?.data!!) {
                showSnackBarMessage(container)
            } else {
                showSnackBarMessage(container, R.string.money_sent_success)
            }
        })

        viewModel.validValue.observe(this, Observer {
            if (!it!!) {
                dialog.dismiss()
                showSnackBarMessage(container, R.string.money_invalid_value)
            }
        })
    }

    private fun initRecycler() {
        val layoutManager = LinearLayoutManager(recyclerView.context)
        val dividerItemDecoration = DividerItemDecoration(recyclerView.context, layoutManager.orientation)

        sendMoneyRecyclerAdapter.listener = this
        recyclerView.adapter = sendMoneyRecyclerAdapter
        recyclerView.addItemDecoration(dividerItemDecoration)
        recyclerView.layoutManager = layoutManager
    }

    private fun initDialog() {
        dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog)
        dialog.window.attributes.windowAnimations = R.style.DialogAnimation
        dialog.window.setBackgroundDrawableResource(R.drawable.rounded_corner)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)

        val width = (resources.displayMetrics.widthPixels * 0.90).toInt()
        val height = (resources.displayMetrics.heightPixels * 0.60).toInt()
        dialog.window.setLayout(width, height)
    }

    private fun changeLoadingVisibilty(visibility: Boolean) {
        if (visibility) {
            loadingContainer.visibility = View.VISIBLE
            dialog.dismiss()
            return
        }
        loadingContainer.visibility = View.GONE
    }

    private fun fillRecyclerView(friendList: List<Friend>?) {
        sendMoneyRecyclerAdapter.addToList(friendList as ArrayList<Friend>)
    }

    override fun onItemClick(view: View, position: Int) {
        showSendDialog(sendMoneyRecyclerAdapter.getItem(position))
    }

    private fun showSendDialog(friend: Friend) {
        val name: AppCompatTextView = dialog.findViewById(R.id.dialog_profile_name)
        val phone: AppCompatTextView = dialog.findViewById(R.id.dialog_profile_phone)
        val close: AppCompatImageView = dialog.findViewById(R.id.dialog_close)
        val value: AppCompatEditText = dialog.findViewById(R.id.dialog_value)
        val sendBtn: AppCompatButton = dialog.findViewById(R.id.dialog_send_btn)

        value.setText(R.string.zero_value)

        value.addTextChangedListener(MonetaryMask(value))

        close.setOnClickListener { dialog.dismiss() }

        sendBtn.setOnClickListener { viewModel.sendMoneyToFriend(friend.id, value.text.toString()) }

        name.text = friend.name
        phone.text = friend.phone

        dialog.show()
    }
}