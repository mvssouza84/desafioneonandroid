package marcus.desafio.banconeon.desafioneonandroid.data.model

import com.squareup.moshi.Json


data class SendMoney(
        @field:Json(name = "ClienteId") val clienteId: Int,
        @field:Json(name = "token") val token: String,
        @field:Json(name = "valor") val value: Double
)