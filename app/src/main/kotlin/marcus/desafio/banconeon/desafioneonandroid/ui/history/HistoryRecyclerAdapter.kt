package marcus.desafio.banconeon.desafioneonandroid.ui.history

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import concrete.marcussouza.desafioandroid.ui.BaseRecyclerAdapter
import marcus.desafio.banconeon.desafioneonandroid.R
import marcus.desafio.banconeon.desafioneonandroid.data.model.TransferUser
import java.text.NumberFormat
import java.util.*

class HistoryRecyclerAdapter : BaseRecyclerAdapter<TransferUser>() {

    private val numberFormat: NumberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault())

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        (holder as ViewHolder).bind(recyclerList[position], numberFormat)
    }

    override fun getItemCount(): Int {
        return recyclerList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_history_friend, parent, false)
        return ViewHolder(view)
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        private val container: ConstraintLayout = view.findViewById(R.id.item_history_friend_container)
        private val name: AppCompatTextView = view.findViewById(R.id.item_history_friend_name)
        private val phone: AppCompatTextView = view.findViewById(R.id.item_history_friend_phone)
        private val value: AppCompatTextView = view.findViewById(R.id.item_history_friend_value)

        fun bind(transferUser: TransferUser, numberFormat: NumberFormat) {
            name.text = transferUser.friend.name
            phone.text = transferUser.friend.phone
            value.text = numberFormat.format(transferUser.value)
        }
    }
}