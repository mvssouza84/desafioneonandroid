package marcus.desafio.banconeon.desafioneonandroid.data.model

import com.squareup.moshi.Json


data class FriendList(
        @field:Json(name = "list") val friendList: List<Friend>
)

data class Friend(
        @field:Json(name = "id") val id: Int,
        @field:Json(name = "name") val name: String,
        @field:Json(name = "phone") val phone: String,
        @field:Json(name = "photo_url") val photoUrl: String
)