package marcus.desafio.banconeon.desafioneonandroid.ui.history

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.IAxisValueFormatter


class HistoryChartFormatter(private val labelList: ArrayList<String>) : IAxisValueFormatter {


    override fun getFormattedValue(value: Float, axis: AxisBase?): String {
        return labelList[value.toInt()]
    }
}