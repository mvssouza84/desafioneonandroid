package marcus.desafio.banconeon.desafioneonandroid.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import marcus.desafio.banconeon.desafioneonandroid.ui.history.HistoryViewModel
import marcus.desafio.banconeon.desafioneonandroid.ui.home.HomeViewModel
import marcus.desafio.banconeon.desafioneonandroid.ui.send.SendMoneyViewModel


@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SendMoneyViewModel::class)
    abstract fun bindSendMoneyViewModel(viewModel: SendMoneyViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HistoryViewModel::class)
    abstract fun bindSHistoryViewModel(viewModel: HistoryViewModel): ViewModel

}