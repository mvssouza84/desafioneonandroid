package concrete.marcussouza.desafioandroid.ui

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import marcus.desafio.banconeon.desafioneonandroid.data.api.Api
import marcus.desafio.banconeon.desafioneonandroid.data.api.Local


abstract class BaseViewModel(val api: Api, val local: Local) : ViewModel() {

    var disposables = CompositeDisposable()

    val loading = MutableLiveData<Boolean>()

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }
}