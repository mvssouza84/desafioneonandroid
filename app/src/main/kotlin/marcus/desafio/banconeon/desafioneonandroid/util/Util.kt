package marcus.desafio.banconeon.desafioneonandroid.util

import java.io.IOException
import java.io.InputStream


class Util {

    companion object {
        fun inputStreamToString(inputStream: InputStream): String {
            try {
                val bytes = ByteArray(inputStream.available())
                inputStream.read(bytes, 0, bytes.size)
                return String(bytes)
            } catch (e: IOException) {
            }
            return ""
        }
    }
}