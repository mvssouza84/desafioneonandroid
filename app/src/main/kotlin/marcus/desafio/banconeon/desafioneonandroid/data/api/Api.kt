package marcus.desafio.banconeon.desafioneonandroid.data.api

import io.reactivex.Observable
import marcus.desafio.banconeon.desafioneonandroid.data.model.SendMoney
import marcus.desafio.banconeon.desafioneonandroid.data.model.Transfer
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query


interface Api {

    @GET("GenerateToken")
    fun getUserToken(@Query("nome") nome: String, @Query("email") email: String): Observable<String>

    @POST("SendMoney")
    fun sendMoney(@Body sendMoney: SendMoney): Observable<Boolean>

    @GET("GetTransfers")
    fun getTransfers(@Query("token") token: String): Observable<List<Transfer>>
}