package marcus.desafio.banconeon.desafioneonandroid.ui.home

import android.arch.lifecycle.MutableLiveData
import concrete.marcussouza.desafioandroid.ui.BaseViewModel
import concrete.marcussouza.desafioandroid.util.ResourceLiveData
import concrete.marcussouza.desafioandroid.util.toErrorHandler
import marcus.desafio.banconeon.desafioneonandroid.data.api.Api
import marcus.desafio.banconeon.desafioneonandroid.data.api.Local
import javax.inject.Inject


class HomeViewModel @Inject constructor(api: Api, local: Local) : BaseViewModel(api, local) {

    val tokenRresult = ResourceLiveData<String>()
    val btnVisibility = MutableLiveData<Boolean>()

    fun getUserToken(name: String, email: String) {
        api.getUserToken(name, email)
                .doOnSubscribe {
                    loading.postValue(true)
                    btnVisibility.postValue(false)
                }
                .doFinally { loading.postValue(false) }
                .toErrorHandler()
                .subscribeLiveData(this, tokenRresult)
    }

    fun saveToken(token: String) {
        local.saveToken(token)
    }

    fun getToken(): String = local.getToken()

    fun changeBtnVisibilty(visiblity: Boolean) {
        btnVisibility.postValue(visiblity)
    }
}