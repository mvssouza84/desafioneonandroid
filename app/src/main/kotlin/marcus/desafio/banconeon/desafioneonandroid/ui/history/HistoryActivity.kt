package marcus.desafio.banconeon.desafioneonandroid.ui.history

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.graphics.Color
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.FrameLayout
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.utils.ColorTemplate
import concrete.marcussouza.desafioandroid.ui.BaseActivity
import concrete.marcussouza.desafioandroid.util.bindView
import marcus.desafio.banconeon.desafioneonandroid.R
import marcus.desafio.banconeon.desafioneonandroid.data.model.TransferUser
import javax.inject.Inject


class HistoryActivity : BaseActivity(), View.OnClickListener {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)
                .get(HistoryViewModel::class.java)
    }

    private val historyRecyclerAdapter by lazy { HistoryRecyclerAdapter() }

    private val toolbar: Toolbar by bindView(R.id.activity_history_toolbar)
    private val loadingContainer: FrameLayout by bindView(R.id.activity_history_loading_container)
    private val container: ConstraintLayout by bindView(R.id.activity_history_container)
    private val recyclerView: RecyclerView by bindView(R.id.activity_history_recycler)
    private val chart: BarChart by bindView(R.id.activity_history_chart)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_history)
        setupToolbar()
        initObservers()
        initRecycler()
        initChart()

        viewModel.getTransfers(applicationContext.resources.openRawResource(R.raw.friends_list))
    }

    private fun setupToolbar() {
        toolbar.background.alpha = 0
        setSupportActionBar(toolbar)
        supportActionBar?.title = resources.getString(R.string.history_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initObservers() {
        viewModel.transfersResult.observeResource(this, onSuccess = {
            fillChart(it)
            fillRecyclerView(it)
        }, onError = {
            showSnackBarError(container, this)
        })

        viewModel.loading.observe(this, Observer {
            changeLoadingVisibilty(it!!)
        })
    }

    private fun initRecycler() {
        val layoutManager = LinearLayoutManager(recyclerView.context)
        val dividerItemDecoration = DividerItemDecoration(recyclerView.context, layoutManager.orientation)

        recyclerView.adapter = historyRecyclerAdapter
        recyclerView.addItemDecoration(dividerItemDecoration)
        recyclerView.layoutManager = layoutManager
    }

    private fun initChart() {
        chart.setDrawBarShadow(false)
        chart.setDrawValueAboveBar(true)

        chart.description.isEnabled = false
        chart.animateXY(2000, 2000)
    }

    private fun fillRecyclerView(transferUserList: List<TransferUser>?) {
        historyRecyclerAdapter.addToList(transferUserList as ArrayList<TransferUser>)
    }

    private fun fillChart(transferUserList: List<TransferUser>?) {
        val barEntrieList = ArrayList<BarEntry>()
        val labels = ArrayList<String>()

        var position = 0

        for ((friend, value) in transferUserList as ArrayList) {
            barEntrieList.add(BarEntry(position.toFloat(), value.toFloat()))
            labels.add(friend.name)
            position += 1
        }

        val barChartDataSet = BarDataSet(barEntrieList, "Amigos")
        barChartDataSet.setColors(*ColorTemplate.MATERIAL_COLORS)
        barChartDataSet.valueTextColor = Color.WHITE
        barChartDataSet.valueTextSize = 16f

        customizeChart(labels)

        val barData = BarData(barChartDataSet)
        chart.data = barData

        chart.fitScreen()
        chart.zoom((labels.size / 3.25).toFloat(), 1f, 1f, 1f)
        chart.invalidate()
    }

    private fun customizeChart(labels: ArrayList<String>) {
        val xAxisFormatter = HistoryChartFormatter(labels)

        val xAxis = chart.xAxis
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawGridLines(false)
        xAxis.granularity = 1f
        xAxis.labelCount = labels.size
        xAxis.textColor = Color.WHITE
        xAxis.valueFormatter = xAxisFormatter
        xAxis.textSize = 12f

        chart.axisLeft.isEnabled = false
        chart.axisRight.isEnabled = false
        chart.legend.isEnabled = false
    }

    private fun changeLoadingVisibilty(visibility: Boolean) {
        if (visibility) {
            loadingContainer.visibility = View.VISIBLE
            return
        }
        loadingContainer.visibility = View.GONE
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            android.support.design.R.id.snackbar_action -> {
                viewModel.getTransfers(applicationContext.resources.openRawResource(R.raw.friends_list))
            }
        }
    }
}