package marcus.desafio.banconeon.desafioneonandroid.util

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class CustomDateAdapter {

    private val dateFormat: DateFormat

    init {
        dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.getDefault())
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"))
    }

    @ToJson
    @Synchronized internal fun dateToJson(d: Date): String {
        return dateFormat.format(d)
    }

    @FromJson
    @Synchronized
    @Throws(ParseException::class)
    internal fun dateToJson(s: String): Date {
        return dateFormat.parse(s)
    }
}