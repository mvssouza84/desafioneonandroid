package marcus.desafio.banconeon.desafioneonandroid.ui.send

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import concrete.marcussouza.desafioandroid.ui.BaseRecyclerAdapter
import marcus.desafio.banconeon.desafioneonandroid.R
import marcus.desafio.banconeon.desafioneonandroid.data.model.Friend

class SendMoneyRecyclerAdapter() : BaseRecyclerAdapter<Friend>() {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        (holder as ViewHolder).bind(recyclerList[position])
    }

    override fun getItemCount(): Int {
        return recyclerList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_friend, parent, false)
        return ViewHolder(view, listener)
    }

    class ViewHolder(val view: View, val listener: OnItemClickListener?) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private val container: ConstraintLayout = view.findViewById(R.id.item_friend_container)
        private val name: AppCompatTextView = view.findViewById(R.id.item_friend_name)
        private val phone: AppCompatTextView = view.findViewById(R.id.item_friend_phone)

        init {
            container.setOnClickListener(this)
        }

        fun bind(friend: Friend) {
            name.text = friend.name
            phone.text = friend.phone
        }

        override fun onClick(view: View) {
            listener?.onItemClick(view, adapterPosition)
        }
    }
}