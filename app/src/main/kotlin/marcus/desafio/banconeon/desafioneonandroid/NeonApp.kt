package marcus.desafio.banconeon.desafioneonandroid

import android.app.Activity
import android.app.Application
import com.orhanobut.hawk.Hawk
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import marcus.desafio.banconeon.desafioneonandroid.di.AppInjector
import marcus.desafio.banconeon.desafioneonandroid.di.DaggerAppComponent
import javax.inject.Inject

open class NeonApp : Application(), HasActivityInjector {
    @Inject lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()

        AppInjector.init(this, createAppComponent())
        Hawk.init(this).build()
    }

    open fun createAppComponent() = DaggerAppComponent.builder()
            .application(this)
            .build()

    override fun activityInjector() = activityInjector
}