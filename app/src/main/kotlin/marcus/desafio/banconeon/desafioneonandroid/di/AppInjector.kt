package marcus.desafio.banconeon.desafioneonandroid.di

import android.app.Activity
import android.app.Application
import android.os.Bundle
import marcus.desafio.banconeon.desafioneonandroid.NeonApp


object AppInjector {

    fun init(app: NeonApp, component: AppComponent) {
        component.inject(app)

        app.registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {

            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {}

            override fun onActivityPaused(activity: Activity) {}

            override fun onActivityResumed(activity: Activity) {}

            override fun onActivityStarted(activity: Activity) {}

            override fun onActivityDestroyed(activity: Activity) {}

            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {}

            override fun onActivityStopped(activity: Activity) {}
        })
    }
}