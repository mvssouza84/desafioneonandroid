package marcus.desafio.banconeon.desafioneonandroid.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import marcus.desafio.banconeon.desafioneonandroid.ui.history.HistoryActivity
import marcus.desafio.banconeon.desafioneonandroid.ui.home.HomeActivity
import marcus.desafio.banconeon.desafioneonandroid.ui.send.SendMoneyActivity

@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector
    abstract fun provideHomeActivityInjector(): HomeActivity

    @ContributesAndroidInjector
    abstract fun provideSendMoneyActivityInjector(): SendMoneyActivity

    @ContributesAndroidInjector
    abstract fun provideHistoryActivityInjector(): HistoryActivity
}