package marcus.desafio.banconeon.desafioneonandroid.di

import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import marcus.desafio.banconeon.desafioneonandroid.BuildConfig
import marcus.desafio.banconeon.desafioneonandroid.data.api.Api
import marcus.desafio.banconeon.desafioneonandroid.data.api.Local
import marcus.desafio.banconeon.desafioneonandroid.util.CustomDateAdapter
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton


@Module
open class AppModule {

    @Provides
    @Singleton
    fun provideMoshi() = Moshi.Builder().add(CustomDateAdapter()).build()

    @Provides
    @Singleton
    open fun provideOkHttpClientBuilder(interceptors: Set<@JvmSuppressWildcards Interceptor>): OkHttpClient.Builder {
        val builder = OkHttpClient.Builder()
        interceptors.forEach { builder.addInterceptor(it) }
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        }
        return builder
    }

    @Provides
    @Singleton
    fun provideApi(client: OkHttpClient, moshi: Moshi, @Named("HTTP_URL") url: String): Api {
        return Retrofit.Builder()
                .client(client)
                .baseUrl(url)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .build()
                .create(Api::class.java)
    }

    @Provides
    @Singleton
    fun provideLocal(): Local = Local()


}