package marcus.desafio.banconeon.desafioneonandroid.data.model

import com.squareup.moshi.Json
import java.util.*


data class Transfer(
        @field:Json(name = "id") val id: Int,
        @field:Json(name = "ClienteId") val clientId: Int,
        @field:Json(name = "Valor") val value: Double,
        @field:Json(name = "Token") val token: String,
        @field:Json(name = "Data") val date: Date
)

data class TransferUser(
        @field:Json(name = "friend") val friend: Friend,
        @field:Json(name = "value") val value: Double,
        @field:Json(name = "date") val date: Date
)