package marcus.desafio.banconeon.desafioneonandroid.di

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import marcus.desafio.banconeon.desafioneonandroid.NeonApp
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(
        AndroidInjectionModule::class,
        AppModule::class,
        ViewModelModule::class,
        ActivitiesModule::class,
        NetworkModule::class
))
interface AppComponent {

    fun inject(app: NeonApp)

    @Component.Builder
    interface Builder {

        fun build(): AppComponent

        @BindsInstance
        fun application(app: NeonApp): Builder
    }
}