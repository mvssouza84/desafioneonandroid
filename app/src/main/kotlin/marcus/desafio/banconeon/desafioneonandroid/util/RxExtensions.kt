package concrete.marcussouza.desafioandroid.util

import android.arch.lifecycle.MutableLiveData
import concrete.marcussouza.desafioandroid.ui.BaseViewModel
import io.reactivex.Observable
import marcus.desafio.banconeon.desafioneonandroid.data.Resource

fun <T> Observable<T>.toErrorHandler() = ErrorHandlerObservable(this)


data class ErrorHandlerObservable<T>(val observable: Observable<T>) {

    fun subscribeLiveData(viewModel: BaseViewModel, liveData: ResourceLiveData<T>) {
        viewModel.disposables.add(
                observable
                        .doOnError({ viewModel.loading.postValue(false) })
                        .subscribe(
                                { liveData.postValue(Resource.success(it)) },
                                { handleError(liveData, it) }))
    }

    private fun handleError(liveData: MutableLiveData<Resource<T>>, it: Throwable) {
        liveData.postValue(Resource.error(it))
    }
}