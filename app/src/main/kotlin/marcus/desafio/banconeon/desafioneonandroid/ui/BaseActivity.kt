package concrete.marcussouza.desafioandroid.ui

import android.arch.lifecycle.LifecycleRegistry
import android.arch.lifecycle.LifecycleRegistryOwner
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import dagger.android.AndroidInjection
import marcus.desafio.banconeon.desafioneonandroid.R


abstract class BaseActivity : AppCompatActivity(), LifecycleRegistryOwner {

    private val lifecycleRegistry = LifecycleRegistry(this)

    override fun getLifecycle() = lifecycleRegistry

    lateinit var snackBarError: Snackbar

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    fun showSnackBarError(view: View, listener: View.OnClickListener, duration: Int = Snackbar.LENGTH_INDEFINITE) {
        snackBarError = Snackbar.make(view, R.string.snack_error_msg, duration)
        snackBarError.setAction(R.string.snack_error_retry_btn, listener)
        snackBarError.show()
    }

    fun showSnackBarMessage(view: View, message: Int = R.string.snack_error_msg, duration: Int = Snackbar.LENGTH_LONG) {
        snackBarError = Snackbar.make(view, message, duration)
        snackBarError.show()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}