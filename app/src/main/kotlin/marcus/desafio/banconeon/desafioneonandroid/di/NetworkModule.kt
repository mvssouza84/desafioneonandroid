package marcus.desafio.banconeon.desafioneonandroid.di

import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoSet
import marcus.desafio.banconeon.desafioneonandroid.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    @Named("HTTP_URL")
    fun provideHttpUrl() = BuildConfig.URL

    @Provides
    @Singleton
    fun provideOkHttpClient(builder: OkHttpClient.Builder) = builder.build()

    @Provides
    @IntoSet
    fun provideInterceptor() = Interceptor { chain ->
        val request = chain.request()

        chain.proceed(request)
    }
}