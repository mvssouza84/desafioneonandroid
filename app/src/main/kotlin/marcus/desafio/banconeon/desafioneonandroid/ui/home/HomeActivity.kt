package marcus.desafio.banconeon.desafioneonandroid.ui.home

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.widget.NestedScrollView
import android.support.v7.widget.AppCompatButton
import android.view.View
import android.widget.FrameLayout
import concrete.marcussouza.desafioandroid.ui.BaseActivity
import concrete.marcussouza.desafioandroid.util.bindView
import marcus.desafio.banconeon.desafioneonandroid.R
import marcus.desafio.banconeon.desafioneonandroid.ui.history.HistoryActivity
import marcus.desafio.banconeon.desafioneonandroid.ui.send.SendMoneyActivity
import javax.inject.Inject


class HomeActivity : BaseActivity(), View.OnClickListener {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)
                .get(HomeViewModel::class.java)
    }

    private val container: NestedScrollView by bindView(R.id.activity_home_container)
    private val sendMoneyBtn: AppCompatButton by bindView(R.id.activity_home_snd_money_btn)
    private val historyBtn: AppCompatButton by bindView(R.id.activity_home_history_btn)
    private val loadingContainer: FrameLayout by bindView(R.id.activity_home_loading_container)
    private lateinit var name: String
    private lateinit var email: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setupListeners()
        initObservers()

        getUserToken()
    }

    private fun setupListeners() {
        sendMoneyBtn.setOnClickListener(this)
        historyBtn.setOnClickListener(this)
    }

    private fun initObservers() {
        viewModel.tokenRresult.observeResource(this, onSuccess = {
            viewModel.saveToken(it)
            viewModel.changeBtnVisibilty(true)
        }, onError = {
            showSnackBarError(container, this)
            viewModel.changeBtnVisibilty(false)
        })

        viewModel.btnVisibility.observe(this, Observer {
            changeBtnsVisibility(it!!)
        })

        viewModel.loading.observe(this, Observer {
            changeLoadingVisibilty(it!!)
        })
    }

    private fun getUserToken() {
        name = resources.getString(R.string.profile_name)
        email = resources.getString(R.string.profile_email)

        viewModel.getUserToken(name, email)
    }

    private fun changeBtnsVisibility(visibility: Boolean) {
        if (visibility) {
            sendMoneyBtn.visibility = View.VISIBLE
            historyBtn.visibility = View.VISIBLE
            return
        }
        sendMoneyBtn.visibility = View.GONE
        historyBtn.visibility = View.GONE
    }

    private fun changeLoadingVisibilty(visibility: Boolean) {
        if (visibility) {
            loadingContainer.visibility = View.VISIBLE
            return
        }
        loadingContainer.visibility = View.GONE
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            android.support.design.R.id.snackbar_action -> {
                viewModel.getUserToken(name, email)
            }
            R.id.activity_home_snd_money_btn -> {
                goToSendMoney()
            }
            R.id.activity_home_history_btn -> {
                gotToHistory()
            }
        }
    }

    private fun goToSendMoney() {
        sendMoneyBtn
                .animate()
                .apply {
                    duration = 50
                    scaleX(0.8f)
                    withEndAction {
                        duration = 50
                        scaleX(1f)
                        withEndAction {
                            startActivity(Intent(applicationContext, SendMoneyActivity::class.java))
                        }
                    }
                    start()
                }
    }

    private fun gotToHistory() {
        historyBtn
                .animate()
                .apply {
                    duration = 50
                    scaleX(0.8f)
                    withEndAction {
                        duration = 50
                        scaleX(1f)
                        withEndAction {
                            startActivity(Intent(applicationContext, HistoryActivity::class.java))
                        }
                    }
                    start()
                }
    }
}