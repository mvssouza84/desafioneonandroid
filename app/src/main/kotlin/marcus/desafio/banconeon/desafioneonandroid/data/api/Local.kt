package marcus.desafio.banconeon.desafioneonandroid.data.api

import com.orhanobut.hawk.Hawk


open class Local {

    fun saveToken(token: String) {
        Hawk.put("token", token)
    }

    fun getToken(): String {
        return Hawk.get("token")
    }
}