package marcus.desafio.banconeon.desafioneonandroid.ui.history

import com.squareup.moshi.Moshi
import concrete.marcussouza.desafioandroid.ui.BaseViewModel
import concrete.marcussouza.desafioandroid.util.ResourceLiveData
import concrete.marcussouza.desafioandroid.util.toErrorHandler
import marcus.desafio.banconeon.desafioneonandroid.data.api.Api
import marcus.desafio.banconeon.desafioneonandroid.data.api.Local
import marcus.desafio.banconeon.desafioneonandroid.data.model.Friend
import marcus.desafio.banconeon.desafioneonandroid.data.model.FriendList
import marcus.desafio.banconeon.desafioneonandroid.data.model.Transfer
import marcus.desafio.banconeon.desafioneonandroid.data.model.TransferUser
import marcus.desafio.banconeon.desafioneonandroid.util.Util
import java.io.InputStream
import javax.inject.Inject


class HistoryViewModel @Inject constructor(api: Api, local: Local, private val moshi: Moshi) : BaseViewModel(api, local) {

    val transfersResult = ResourceLiveData<ArrayList<TransferUser>>()
    private val transferUserList = ArrayList<TransferUser>()
    private var friendList: ArrayList<Friend>? = null


    fun getTransfers(openRawResource: InputStream) {
        api.getTransfers(getToken())
                .doOnSubscribe { loading.postValue(true) }
                .doFinally { loading.postValue(false) }
                .flatMapIterable { transfers -> transfers }
                .map { transfer ->
                    val transferUser = mapToTransferUser(transfer, openRawResource)
                    transferUser?.let { transferUserList.add(it) }
                    return@map transferUserList
                }
                .toErrorHandler()
                .subscribeLiveData(this, transfersResult)
    }

    private fun getToken(): String = local.getToken()

    private fun mapToTransferUser(transfer: Transfer, openRawResource: InputStream): TransferUser? {
        val friend = getLocalFriend(transfer.clientId, openRawResource)

        return if (friend != null) {
            TransferUser(friend, transfer.value, transfer.date)
        } else {
            null
        }
    }

    private fun getLocalFriend(clientId: Int, openRawResource: InputStream): Friend? {
        if (friendList == null) {
            friendList = ArrayList()
            val friendString = Util.inputStreamToString(openRawResource)
            val jsonAdapter = moshi.adapter(FriendList::class.java)
            val friends = jsonAdapter.fromJson(friendString)
            friendList = friends.friendList as ArrayList<Friend>
        }


        return friendList!!.firstOrNull { it.id == clientId }
    }
}