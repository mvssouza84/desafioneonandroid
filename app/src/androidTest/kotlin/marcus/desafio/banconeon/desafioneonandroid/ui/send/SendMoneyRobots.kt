package marcus.desafio.banconeon.desafioneonandroid.ui.send

import android.content.Intent
import android.support.annotation.IntegerRes
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.LayoutAssertions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.v7.widget.RecyclerView
import marcus.desafio.banconeon.desafioneonandroid.R
import marcus.desafio.banconeon.desafioneonandroid.util.RecyclerViewItemCountAssertion
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.Matchers.allOf

class SendMoneyRobots(var rule: ActivityTestRule<SendMoneyActivity>, val server: MockWebServer) {

    fun initActivity(intent: Intent) {
        rule.launchActivity(intent)
    }

    fun validateIfNoOverlapView() {
        onView(withId(R.id.activity_send_money_container)).check(LayoutAssertions.noOverlaps())
    }

    fun validateRecyclerSize(size: Int) {
        onView(withId(R.id.activity_send_money_recycler)).check(RecyclerViewItemCountAssertion(size))
    }

    fun validateViewWithTextDisplayed(@IntegerRes id: Int, value: String) {
        onView(allOf(withId(id), ViewMatchers.withText(value))).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    fun clickOnItem(position: Int) {
        onView(withId(R.id.activity_send_money_recycler)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(position, click()))
    }

    fun clickOnView(@IntegerRes id: Int) {
        onView(withId(id)).perform(click())
    }
}