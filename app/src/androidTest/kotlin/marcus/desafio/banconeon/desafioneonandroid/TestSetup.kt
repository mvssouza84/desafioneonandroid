package marcus.desafio.banconeon.desafioneonandroid


import android.app.Application
import android.content.Context
import android.support.test.espresso.IdlingResource
import android.support.test.runner.AndroidJUnitRunner
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjectionModule
import dagger.multibindings.IntoSet
import marcus.desafio.banconeon.desafioneonandroid.di.ActivitiesModule
import marcus.desafio.banconeon.desafioneonandroid.di.AppComponent
import marcus.desafio.banconeon.desafioneonandroid.di.AppModule
import marcus.desafio.banconeon.desafioneonandroid.di.ViewModelModule
import okhttp3.Dispatcher
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import javax.inject.Named
import javax.inject.Singleton

class TestRunner : AndroidJUnitRunner() {
    override fun newApplication(cl: ClassLoader, className: String, context: Context): Application {
        return super.newApplication(cl, TestApp::class.java.name, context)
    }
}

class TestApp : NeonApp() {

    override fun createAppComponent() =
            DaggerTestAppComponent.builder()
                    .application(this)
                    .build()
}

@Singleton
@Component(modules = arrayOf(
        AndroidInjectionModule::class,
        AppModule::class,
        ViewModelModule::class,
        ActivitiesModule::class,
        TestNetworkModule::class
))
interface TestAppComponent : AppComponent {

    @Component.Builder
    interface Builder {
        fun build(): AppComponent
        @BindsInstance
        fun application(app: TestApp): Builder
    }
}

object TestServerUrl {
    var url: HttpUrl? = null
}

@Module
class TestNetworkModule {

    /* Force HTTP scheme for testing */
    @Provides
    @Singleton
    @Named("HTTP_URL")
    fun providesHttpUrl() = "http://localhost/"

    @Provides
    @Singleton
    fun providesOkHttpClient(builder: OkHttpClient.Builder): OkHttpClient {
        val client = builder.build()
        OkHttp3IdlingResource("billing", client.dispatcher())
        return client
    }

    @Provides
    @IntoSet
    fun providesMockWebServerInterceptor(): Interceptor {
        return Interceptor {
            var request = it.request()
            TestServerUrl.url?.let {
                request = request.newBuilder()
                        .url(request.url()
                                .newBuilder()
                                .host(TestServerUrl.url!!.host())
                                .port(TestServerUrl.url!!.port())
                                .build())
                        .build()
            }
            it.proceed(request)
        }
    }
}

class OkHttp3IdlingResource(val resourceName: String, val dispatcher: Dispatcher) : IdlingResource {

    @Volatile internal var callback: IdlingResource.ResourceCallback? = null

    init {
        dispatcher.setIdleCallback {
            val callback = this@OkHttp3IdlingResource.callback
            callback?.onTransitionToIdle()
        }
    }

    override fun getName() = resourceName
    override fun isIdleNow() = dispatcher.runningCallsCount() == 0

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback) {
        this.callback = callback
    }
}
