package marcus.desafio.banconeon.desafioneonandroid.ui.send

import android.content.Intent
import android.support.test.rule.ActivityTestRule
import marcus.desafio.banconeon.desafioneonandroid.R
import marcus.desafio.banconeon.desafioneonandroid.TestServerUrl
import okhttp3.mockwebserver.MockWebServer
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class SendMoneyActivityTest {

    @Rule
    @JvmField
    val rule = ActivityTestRule<SendMoneyActivity>(SendMoneyActivity::class.java, false, false)

    @Rule
    @JvmField
    val server: MockWebServer = MockWebServer()

    private fun robots(func: SendMoneyRobots.() -> Unit) = SendMoneyRobots(rule, server).apply(func)

    private val intent = Intent()

    @Before
    fun setup() {
        TestServerUrl.url = server.url("/")
    }

    @Test
    fun testIfThereIsNoOverlapViews() {
        robots {
            initActivity(intent)
            validateIfNoOverlapView()
        }
    }

    @Test
    fun testIfScreenLoadWithSuccess() {
        robots {
            initActivity(intent)
            validateRecyclerSize(15)
        }
    }

    @Test
    fun testIfClickOnItemDialogShow() {
        robots {
            initActivity(intent)
            clickOnItem(2)
            validateViewWithTextDisplayed(R.id.dialog_profile_name, "Amigo 3")
        }
    }

    @Test
    fun testIfSendMoneyIsGretterThanZero() {
        robots {
            initActivity(intent)
            clickOnItem(2)
            clickOnView(R.id.dialog_send_btn)
            validateViewWithTextDisplayed(android.support.design.R.id.snackbar_text, "Valor Incorreto! Precisa informar um valor maior!")
        }
    }
}