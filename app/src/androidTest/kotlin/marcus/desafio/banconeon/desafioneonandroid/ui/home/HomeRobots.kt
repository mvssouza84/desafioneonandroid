package marcus.desafio.banconeon.desafioneonandroid.ui.home

import android.content.Intent
import android.support.annotation.IntegerRes
import android.support.test.InstrumentationRegistry.getInstrumentation
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.LayoutAssertions.noOverlaps
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import marcus.desafio.banconeon.desafioneonandroid.R
import marcus.desafio.banconeon.desafioneonandroid.util.TestHelper
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.Matchers.allOf


class HomeRobots(var rule: ActivityTestRule<HomeActivity>, val server: MockWebServer) {

    fun initActivity(intent: Intent) {
        rule.launchActivity(intent)
    }

    fun validateIfNoOverlapView() {
        onView(withId(R.id.activity_home_container)).check(noOverlaps())
    }

    fun mockResponse(fileName: String) {
        server.enqueue(MockResponse()
                .setResponseCode(200)
                .setBody(TestHelper.getStringFromFile(getInstrumentation().context, fileName)))

    }

    fun validateViewWithTextDisplayed(@IntegerRes id: Int, value: String) {
        onView(allOf(withId(id), withText(value))).check(matches(isDisplayed()))
    }

    fun validateViewDisplayed(@IntegerRes id: Int) {
        onView(withId(id)).check(matches(isDisplayed()))
    }

    fun clikOnView(@IntegerRes id: Int) {
        onView(withId(id)).perform(click())
    }

    fun makeDelay(delay: Long) {
        Thread.sleep(delay)
    }
}