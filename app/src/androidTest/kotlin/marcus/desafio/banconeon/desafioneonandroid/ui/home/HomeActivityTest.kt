package marcus.desafio.banconeon.desafioneonandroid.ui.home

import android.content.Intent
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import marcus.desafio.banconeon.desafioneonandroid.R
import marcus.desafio.banconeon.desafioneonandroid.TestServerUrl
import okhttp3.mockwebserver.MockWebServer
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class HomeActivityTest {

    @Rule
    @JvmField
    val rule = ActivityTestRule<HomeActivity>(HomeActivity::class.java, false, false)

    @Rule
    @JvmField
    val server: MockWebServer = MockWebServer()

    private fun robots(func: HomeRobots.() -> Unit) = HomeRobots(rule, server).apply(func)

    private val intent = Intent()

    @Before
    fun setup() {
        TestServerUrl.url = server.url("/")
    }

    @Test
    fun testIfThereIsNoOverlapViews() {
        robots {
            mockResponse("get_user_token_success.json")
            initActivity(intent)
            validateIfNoOverlapView()
        }
    }

    @Test
    fun testIfScreenLoadWithSuccess() {
        robots {
            mockResponse("get_user_token_success.json")
            initActivity(intent)
            validateViewDisplayed(R.id.activity_home_profile_image)
            validateViewWithTextDisplayed(R.id.activity_home_profile_name, "Marcus Souza")
            validateViewWithTextDisplayed(R.id.activity_home_profile_email, "souzamarcusrj@gmail.com")
            validateViewWithTextDisplayed(R.id.activity_home_snd_money_btn, "ENVIAR DINHEIRO")
            validateViewWithTextDisplayed(R.id.activity_home_history_btn, "HISTÓRICO DE ENVIOS")
        }
    }

    @Test
    fun testIfSnackAppearOnError() {
        robots {
            mockResponse("get_user_token_error.json")
            initActivity(intent)
            validateViewWithTextDisplayed(android.support.design.R.id.snackbar_text, "Ocorreu um erro")
        }
    }

    @Test
    fun testIfScreenLoadSuccessAfterSnackBar() {
        robots {
            mockResponse("get_user_token_error.json")
            initActivity(intent)
            makeDelay(1000)
            validateViewWithTextDisplayed(android.support.design.R.id.snackbar_text, "Ocorreu um erro")
            makeDelay(1000)
            mockResponse("get_user_token_success.json")
            clikOnView(android.support.design.R.id.snackbar_action)
            validateViewDisplayed(R.id.activity_home_profile_image)
            validateViewWithTextDisplayed(R.id.activity_home_profile_name, "Marcus Souza")
            validateViewWithTextDisplayed(R.id.activity_home_profile_email, "souzamarcusrj@gmail.com")
            validateViewWithTextDisplayed(R.id.activity_home_snd_money_btn, "ENVIAR DINHEIRO")
            validateViewWithTextDisplayed(R.id.activity_home_history_btn, "HISTÓRICO DE ENVIOS")
        }
    }
}