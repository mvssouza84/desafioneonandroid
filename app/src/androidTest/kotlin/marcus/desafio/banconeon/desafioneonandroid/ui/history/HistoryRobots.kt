package marcus.desafio.banconeon.desafioneonandroid.ui.history

import android.content.Intent
import android.support.annotation.IntegerRes
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.LayoutAssertions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import marcus.desafio.banconeon.desafioneonandroid.R
import marcus.desafio.banconeon.desafioneonandroid.util.RecyclerViewItemCountAssertion
import marcus.desafio.banconeon.desafioneonandroid.util.TestHelper
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.Matchers.allOf


class HistoryRobots(private val rule: ActivityTestRule<HistoryActivity>, private val server: MockWebServer) {


    fun initActivity(intent: Intent) {
        rule.launchActivity(intent)
    }

    fun validateIfNoOverlapView() {
        onView(withId(R.id.activity_history_container)).check(LayoutAssertions.noOverlaps())
    }

    fun mockResponse(fileName: String) {
        server.enqueue(MockResponse()
                .setResponseCode(200)
                .setBody(TestHelper.getStringFromFile(InstrumentationRegistry.getInstrumentation().context, fileName)))

    }

    fun validateRecyclerSize(size: Int) {
        onView(withId(R.id.activity_history_recycler)).check(RecyclerViewItemCountAssertion(size))
    }

    fun validateViewWithTextDisplayed(@IntegerRes id: Int, value: String) {
        onView(allOf(withId(id), withText(value))).check(ViewAssertions.matches(isDisplayed()))
    }

    fun makeDelay(delay: Long) {
        Thread.sleep(delay)
    }

    fun clikOnView(@IntegerRes id: Int) {
        onView(withId(id)).perform(ViewActions.click())
    }
}