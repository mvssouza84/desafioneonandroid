package marcus.desafio.banconeon.desafioneonandroid.ui.history

import android.content.Intent
import android.support.test.rule.ActivityTestRule
import marcus.desafio.banconeon.desafioneonandroid.TestServerUrl
import okhttp3.mockwebserver.MockWebServer
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class HistoryActivityTest {

    @Rule
    @JvmField
    val rule = ActivityTestRule<HistoryActivity>(HistoryActivity::class.java, false, false)

    @Rule
    @JvmField
    val server: MockWebServer = MockWebServer()

    private fun robots(func: HistoryRobots.() -> Unit) = HistoryRobots(rule, server).apply(func)

    private val intent = Intent()

    @Before
    fun setup() {
        TestServerUrl.url = server.url("/")
    }

    @Test
    fun testIfThereIsNoOverlapViews() {
        robots {
            mockResponse("get_history_success.json")
            initActivity(intent)
            validateIfNoOverlapView()
        }
    }

    @Test
    fun testIfScreenLoadWithSuccess() {
        robots {
            mockResponse("get_history_success.json")
            initActivity(intent)
            validateRecyclerSize(7)
        }
    }

    @Test
    fun testIfSnackBarAppearOnError() {
        robots {
            mockResponse("get_history_error.json")
            initActivity(intent)
            validateViewWithTextDisplayed(android.support.design.R.id.snackbar_text, "Ocorreu um erro")
        }
    }

    @Test
    fun testIfScreenLoadSuccessAfterSnackBar() {
        robots {
            mockResponse("get_history_error.json")
            initActivity(intent)
            makeDelay(1000)
            validateViewWithTextDisplayed(android.support.design.R.id.snackbar_text, "Ocorreu um erro")
            makeDelay(1000)
            mockResponse("get_history_success.json")
            clikOnView(android.support.design.R.id.snackbar_action)
            validateRecyclerSize(7)
        }
    }
}